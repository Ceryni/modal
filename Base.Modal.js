
        // register vars
        let modalButton = $('.modalOpen');
        let modal = $('.modal');
        let close = $('.fa-close');
        let attrib = 'modal';
        let bodyClass = 'modal-js';

        // initiaise styled sidebar
        new SimpleBar($('.modalHold')[0]);

        // on modal click
        modalButton.on('click', ({currentTarget}) => {
            // add a class to the body
            $('body').addClass(bodyClass);
            // get current attrib
            let currentAttrib = $(currentTarget).attr(attrib)
            modal.filter(function () {
                // of the attrib matches the the current active data attrib
                // add a class of 'active'
                return $(this).attr(attrib) === currentAttrib;
            }).addClass('active');
        })
        // on close button click
        close.on('click', ({currentTarget}) => {
            // remove class from the modal
            $(currentTarget).closest(modal).removeClass('active');
            // remove class from the body
            $('body').removeClass(bodyClass);
        })
    