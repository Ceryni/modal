<a modal="login" class="modalOpen button grey hollow"> Learn More <span class="arrowHold"><span class="fa fa-angle-right"></span></span></a>

<div class="modal" modal="login">
    <span class="fa fa-close"></span>
    <div class="modalHold" data-simplebar data-simplebar-auto-hide="false">
        <div class="container">
            <div class="row align-middle align-center">
                <div class="col-12 col-md-12 col-lg-12">
                    <h2>Modal Headline</h2>
                    <p>Modal Content</p>
                </div>
            </div>
        </div>
    </div>
</div>